// ==UserScript==
// @name         CREGInator2
// @namespace    http://tampermonkey.net/
// @version      0.35
// @description  A utility to assist in compiling loadable CREGIs and status updates
// @author       Joe McQuade
// @match        http://172.30.117.10:6060/pfv/clob*
// @grant        none
// @require      http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js
// @require      http://malsup.github.com/jquery.form.js
// ==/UserScript==

// Okay, so first things first - this code is horrible. I did not know Javascript
// when I started. I know a little more now, so might re-write portions of this
// to make it a little less hideous and a little more maintainable. Right now it
// kinda works, though, so let's just see how it goes. 

(function() {

    // Pretty much everything will need to know the MPAN/MPRN
    var meterPoint;

    // This will hold all the operations we need to manipulate the flow.
    // Obviously, these will depend on the flow type, so we set it later.
    var fileOps = {};

    // And this will be an object representing the file we're constructing.
    var outFile = {};

    // We'll need a way to keep track of user initials.
    var userInitials = "";

    // I'll also store the collection of the lines of the file, partly
    // as an optimisation, but also so I don't have to keep typing out a
    // hella long query. We'll set it later.
    var fileLines;

    // A list of file types that CREGInator knows how to work with.
    //    (hopefully this will expand in time)
    var knownFileTypes = ['.CREGI', '.CRE', '.S0004', '.U04'];

    // Determines the file type.
    var getFileType = function () {
        return document.querySelectorAll('.section_heading')[0].innerText.match(/(\.[A-Z0-9]+)\n/, 'i')[1];
    };

    // Here are some functions for playing with dates...
    // First up, we'll need to turn a 1 digit day/month into 2 digit string
    var twoDigitString = function (n) {
        return n > 9 ? "" + n : "0" + n;
    };

    // Now we can get flow-compatible datestamps!
    var currentDateStamp = function () {
        var now = new Date();
        return now.getFullYear()
            + twoDigitString(now.getMonth() + 1)
            + twoDigitString(now.getDate());
    };

    // set the initials from the input box
    var storeInitials = function () {
        userInitials = $('#user-initials').val();
        localStorage.setItem('CREGInatorInitials', userInitials);
    };

    var retrieveInitials = function () {
        userInitials = localStorage.getItem('CREGInatorInitials');
    };

    // Right, now what? The user enters the MP(A|R)N and clicks...
    // this function will handle that.
    var CREGInate = function (event) {

        // set the flow type
        setFileOps();

        // set the value of meterPoint
        meterPoint = $('#mpr-box').val().stripWhitespace();
        outFile.meterPoint = meterPoint; // Hacking around my own bugs...

        if (fileOps.validateInput(meterPoint)) {

            // get the file lines in a format we can actually use.
            setFileLines();


            // set some things that our out file needs to know about
            outFile.fileName = fileOps.getFileName();

            // set the user initials;
            storeInitials();

            // This will be the meat and potatoes.
            // The 'getLines' function in fileOps takes an object ref
            // for our out file and fills it full of the relevant lines.
            // It's done this way so that at some point I can add
            // support for status loading as well.
            fileOps.setLines(outFile);

            // let's actually test that the MPR returns some lines
            if (outFile.lines.length < 1) {
                alert("MPR not found!");
                return;
            }

            // Set the header and footer
            fileOps.setHeader(outFile);
            fileOps.setFooter(outFile);

            // Then we'll need to compile the CREGI and display it.
            displayOutFile();
        }
        event.preventDefault();
        return false;


    };

    var submitCREGI = function () {
        if ($('#mpr-box').val().match(/\d{9}\d+/)) {
            CREGInate();
        }
    };

    var displayOutFile = function () {
        if (!$('#out-file').length) {
            $('#file-output').css({float : 'left' });
            $('#file-output')
                .append($('<textarea id="out-filename" rows="1"></textarea>'));
            $('out-filename').css({
                float : 'left'
            });
            $('#out-filename').val(fileOps.getFileName());
            $('#out-filename').css({ width : '400px' });
            $('#file-output').append('<br>');
            $('#file-output')
                .append($('<textarea id="out-file" rows="15" wrap="soft"></textarea>'));
            $('#out-file').css({
                whiteSpace : 'nowrap',
                overflow : 'scroll',
                overflowX : 'scroll',
                width : '700px'
            });

            $('#out-file').val(compileFileObj(outFile));
            $('#out-file').select();
        }

    };


    // Right, so I actually needed to rewrite this function twice
    // because it turns out that the only thing uglier than my code
    // is PFV's code. Yuck.
    setFileLines = function () {
        var lines = [];

        // get the relevant DOM elements
        var elts = $(".page_text > tbody > tr > td:nth-child(1)")
            .contents();

        // PFV occasionally puts things in bold, which kills the structure
        // of the page. This is the ugliness required to get around it.
        for (i = 0; i < elts.length; i++) {
            if (elts[i].nodeType == 3) {
                lines.push(elts[i].data.replace(/(.+)?[\n\r]/, '$1'));
                // if it's a bold tag, hackily concatenate it onto the last textNode
            } else if (elts[i].nodeType == 1 && elts[i].nodeName == 'B') {
                lines[lines.length - 1] = lines[lines.length - 1] +
                    elts[i].innerText +
                    elts[i + 1].data.replace(/(.+)?[\n\r]/, '$1'); //also strip newlines
                    i++;
            }
        }
        fileLines = lines;
        // seriously, that's hideous. Possibly the ugliest code I've written.
    };

    // This will set the filetype based on the file extension of the flow
    var setFileOps = function () {
        var extension = getFileType();
        console.log(extension);
        if(extension == ".CREGI") {
            fileOps = elecCREGI;
        } else if (extension == ".CRE") {
            fileOps = gasCREGI;
        } else if (extension == ".S0004") {
            fileOps = elecStatus;
        } else if (extension == ".U04") {
            fileOps = gasStatus;
        }
    };

    // -------------------------------------------------------------------------
    // FILE OPS : File operations go here
    // -------------------------------------------------------------------------

    // Now we'll actually define the fileOps for different flow types

    var elecCREGI = {};
    var gasCREGI = {};
    var elecStatus = {}; // I'm not going to concentrate on statuses yet. Sorry!
    var gasStatus = {};

    // INPUT VALIDATION
    String.prototype.stripWhitespace = function () {
        return this.replace(/\s/g, "");
    };

    var validateMPAN = function (input) {
        if (!input.match(/^\d{13}$/)) {
            alert("That doesn't look like an MPAN...");
            return false;
        } else {
            return true;
        }
    };

    var validateMPRN = function (input) {
        if (!input.match(/^\d{10}$/)) {
            alert("That doesn't looke like an MPRN...");
            return false;
        } else {
            return true;
        }
    };

    elecCREGI.validateInput = validateMPAN;
    elecStatus.validateInput = validateMPAN;

    gasCREGI.validateInput = validateMPRN;
    gasStatus.validateInput = validateMPRN;

    // FILE NAME FUNCTIONS

    // get the flow file name and add initials using simple RegExes
    elecCREGI.getFileName = function () {
        var flowFile =  $('.section_heading').text().match(/[A-Z0-9]+\..+/)[0];
        return flowFile.replace(/\.CREGI/, userInitials + ".CREGI");
    };

    // it's the same for gas. Probably should refactor this.
    gasCREGI.getFileName = function () {
        var flowFile =  $('.section_heading').text().match(/[A-Z0-9]+\..+/)[0];
        return flowFile.replace(/\.CRE/, userInitials + ".CRE");
    };

    elecStatus.getFileName = function () {
        return "statusupdate_" + meterPoint + "_" + currentDateStamp() + "_" + userInitials + ".S0004";
    };

    gasStatus.getFileName = function () {
        return "statusupdate_" + meterPoint + "_" + currentDateStamp() + "_" + userInitials + ".U04";
    };

    // FILE LINE FUNCTIONS

    // This function will get a range of lines, starting when it sees one pattern
    // and stopping when it sees another.
    var takeBetween = function (source, startRE, endRE) {
        var taking = false;
        var lines = [];

        for (i = 0; i < source.length; i++) {
            if (taking) {
                if (source[i].match(endRE)) {

                    break;
                } else {
                    lines.push(source[i]);
                }
            } else if (source[i].match(startRE)) {
                taking = true;

                lines.push(source[i]);
            }
        }

        return lines;

    };

    var getLineMatching = function (source, regex) {
        for (i = 0; i < source.length; i++) {
            if (source[i].match(regex)) {
                return source[i];
            }
        }
    };

    elecCREGI.setLines = function (fileObj) {
        var startPattern = new RegExp(fileObj.meterPoint);
        fileObj.lines = takeBetween(fileLines, startPattern, /(C01)|(ZPT)/);
    };

    gasCREGI.setLines = function (fileObj) {
        var startPattern = new RegExp(fileObj.meterPoint);
        fileObj.lines = takeBetween(fileLines, startPattern, /("US010")|"TRAIL"/);
    };

    // a really simple function for sorting statuses. It kinda just assumes
    // that status numbers are  monotonically increasing, whish might be right?
    var compareStatus = function (line1, line2) {
        var statusNum1 = fileOps.getStatusNumber(line1);
        var statusNum2 = fileOps.getStatusNumber(line2);
        return statusNum1 < statusNum2;
    };

    elecStatus.getStatusNumber = function (line) {
        return +line.split(/\|/)[2];
    };

    gasStatus.getStatusNumber = function (line) {
        return +line.split(/,/)[2].match(/[^"]/)[0];
    };


    // For working with statuses, we will need a way to store the lines between
    // clob files. For this, we'll use local storage and file the lines as
    // a tilda-separated string of strings keyed to the meter-point

    var storeLines = function (fileObj) {
        var outString = "";
        var lines = fileObj.lines;

        if (lines.length > 0) {
            outString = lines[0];
            for (i = 1; i < lines.length; i++) {
                outString = outString + "~" + lines[i];
            }

            localStorage.setItem(meterPoint+"LINES", outString);
        }
    };

    var retrieveLines = function (fileObj) {
        var inString = localStorage.getItem(meterPoint + "LINES");
        if (inString) {
            return inString.split('~');
        } else {
            return [];
        }
    };


    gasStatus.setLines = function (fileObj) {
        var prevLines = retrieveLines(fileObj);

        var currentLine = getLineMatching(fileLines,  meterPoint);
        prevLines.push(currentLine);
        fileObj.lines = prevLines;
        storeLines(fileObj);
    };

    elecStatus.setLines = gasStatus.setLines;


    // HEADER FUNCTIONS
    elecCREGI.setHeader = function (fileObj) {
        fileObj.header = fileLines[0];
    };

    gasCREGI.setHeader = function(fileObj) {
        var fileHeader = fileLines[0];

        fileObj.header =
            fileHeader.match(/(([^,]+,){10})/)[0] + fileObj.lines.length + ",1";
    };

    elecStatus.setHeader = function (fileObj) {
        var headMatch = fileLines[0].match(/((?:[A-Z0-9]+\|){7})\d{8}(.+)/);
        fileObj.header = headMatch[1] + currentDateStamp() + headMatch[2];
    };

    gasStatus.setHeader = function (fileObj) {
        fileObj.header = fileLines[0]
            .replace(/((:?.+,){6})\d{8},(:?.+)(,\d+){2}/, "$1"
                     + currentDateStamp() + ",$3," + fileObj.lines.length + ",1");
    };


    // FOOTER FUNCTIONS
    elecCREGI.setFooter = function (fileObj) {
        var fileFooter = fileLines[fileLines.length -1];

        fileObj.footer =
            fileFooter.replace(/([A-Z0-9]+\|[A-Z0-9]+\|)\d+\|\|\d+(.+)/,
                               "$1" + fileObj.lines.length +
                               "||" + "1" + "$2");
    };

    gasCREGI.setFooter = function (fileObj) {
        fileObj.footer = '"TRAIL"';
    };

    elecStatus.setFooter = function (fileObj) {
        var foot = fileLines[fileLines.length - 1];
        
        var footerMatch = foot.match(/(([a-zA-Z0-9]+\|){3}\|).+(\d{6}\|$)/);
        

        fileObj.footer = foot.match(/([a-zA-Z0-9]+\|){2}/)[0] + fileObj.lines.length + "||1|"
            + currentDateStamp() + footerMatch[3];
    };

    gasStatus.setFooter = function (fileObj) {
        fileObj.footer = '"TRAIL"';
    };


    // The last thing to do is to take our file object and turn it into, well,
    // something that actually looks like a file
    var compileFileObj = function (fileObj) {
        var file = fileObj.header + "\n";

        for (i = 0; i < fileObj.lines.length; i++) {
            file = file + fileObj.lines[i].replace(/\xa0/g, ' ') + "\n";
        }
        return file + fileObj.footer;
    };


    // -------------------------------------------------------------------------
    // And here is where the REALLY ugly stuff lives. Actually displaying stuff.
    // -------------------------------------------------------------------------
    // Look, I don't know anything about web programming, okay?

    // A simple template for the top bar
    var topBarTemplate =
        "<div id='creginator'>\
<form id='in-form'>\
Meter point:\
<input type='text' id='mpr-box' autofocus>\
<input id='creginate' type='button' value='CREGInate!'>\
<br><br><div id='file-output'></div>\
</form>\
<div id = 'initials'>\
Initials:\
<input type='text' id='user-initials'>\
</div>\
</div>";

    // Here's our initialisation function
    var docInit = function () {

        // Add our stylesheet
        //$('head').append($('<link>', {
        //    'rel' : 'stylesheet',
        //    'type' : 'text/css',
        //    'href' : '../../test1.css'
        //}));

        // Pop a bar on the top
        $(topBarTemplate).insertBefore('#ent-outerwindow');
        // And style it
        $('#creginator').css({
            borderStyle : "solid",
            borderWidth : "1px",
            backgroundColor : "#737373",
            padding : "5px"
        });

        $('#in-form').css({ float : 'left' });
        $('#initials').css({ float : 'right' });

        retrieveInitials();

        $('#user-initials').val(userInitials);
        $('#creginator').append($('<br>').css({clear : 'both'}));

        // override the style of the native PFV top bar
        $('head').append($('<style type="text/css">div.ent-outerwindow { top : auto !important; }</style>'));

        $('#mpr-box').focus();

        $('#in-form').ajaxForm(submitCREGI);

        $('#creginate').click(submitCREGI);

    };

    // we don't want to bother doing anything unless we know how to operate on
    // this type of file
    var init = function () {
        if (knownFileTypes.indexOf(getFileType()) > -1) {
            //        document.write( '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"><\/script>'); //doesn't work
            docInit();
        }
    };
    // and run it when the clob has finished loading
    document.addEventListener('DOMContentLoaded', init, false);
    init();
})();
